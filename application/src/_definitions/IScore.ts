export interface IScore {
  teamId: string
  score: number
  guessedWords: string[]
  color: string
}
