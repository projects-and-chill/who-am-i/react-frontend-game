export interface IMe {
  id: string
  username: string
  blinded: boolean
  leader: boolean
  timeMaster: boolean
  vote: -1 | 0 | 1
  teamId: string
  roomId: string
  avatar: string
}
