import React, { createContext, Dispatch, FC, ReactNode, SetStateAction, useContext, useMemo, useState } from "react"

type IFirstConnectionContext = {
  firstConnection: boolean
  setFirstConnection: Dispatch<SetStateAction<boolean>> & Dispatch<boolean>
}
type Props = { children?: ReactNode }

const FirstConnectionContext = createContext<IFirstConnectionContext | null>(null)

const FirstConnectionProvider: FC<Props> = ({ children }) => {

  const [firstConnection, setFirstConnection] = useState<boolean>(true)

  const value: IFirstConnectionContext = useMemo(() => ({ firstConnection, setFirstConnection }), [firstConnection])

  return <FirstConnectionContext.Provider value={value}>{children}</FirstConnectionContext.Provider>

}

function useFirstConnection () {

  const firstConnectionContext = useContext(FirstConnectionContext)
  if (!firstConnectionContext)

    throw new Error("useFirstConnection must be used within a FirstConnectionProvider")

  return firstConnectionContext as IFirstConnectionContext

}

export { FirstConnectionProvider, useFirstConnection }
