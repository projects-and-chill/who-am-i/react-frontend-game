import { IScore } from "../../_definitions/IScore"
import { ScoresActions } from "./ScoresActions"
import { Action, State } from "./ScoresContext"

type Reducer = (state: State, action: Action) => State

const ScoreReducer: Reducer = (prevState, action) => {

  switch (action.type) {

  case ScoresActions.ADD: {

    const newScore = action.payload as IScore
    if (prevState.some((score) => score.teamId === newScore.teamId))
      return prevState
    return [...prevState, newScore]

  }
  case ScoresActions.REMOVE: {

    const removedScoreId = action.payload as string
    return prevState.filter((score) => score.teamId !== removedScoreId)

  }
  case ScoresActions.UPDATE: {

    const updScore = action.payload as IScore
    const prevStateCopy = [...prevState]
    const index = prevState.findIndex((score) => score.teamId === updScore.teamId)
    if (index !== -1)
      prevStateCopy[index] = updScore
    return prevStateCopy

  }
  case ScoresActions.UDPATE_ALL:
    return action.payload as IScore[]
  case ScoresActions.RESET_ALL:
    return []
  default:
    throw new Error(`Unhandled action type: ${action.type}`)

  }

}

export { ScoreReducer }
