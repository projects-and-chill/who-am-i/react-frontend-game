import React, {
  createContext,
  FC,
  ReactNode,
  useContext,
  useMemo,
  useReducer
} from "react"
import { ScoreReducer } from "./ScoresReducer"
import { ScoresActions } from "./ScoresActions"
import { IScore } from "../../_definitions/IScore"

type Props = { children?: ReactNode }
type State = IScore[]
type Action = {
  type: ScoresActions
  payload?: unknown
}
type Dispatch = (action: Action) => void
type ScoresContext = {
  scores: State
  setScores: Dispatch
}

const ScoresContext = createContext<ScoresContext | null>(null)

const ScoresProvider: FC<Props> = ({ children }) => {

  const [scores, setScores] = useReducer(ScoreReducer, [])

  const value: ScoresContext = useMemo(() => ({ scores, setScores }), [scores])

  return <ScoresContext.Provider value={value}>{children}</ScoresContext.Provider>

}

function useScores () {

  const scoresContext = useContext(ScoresContext)
  if (!scoresContext)

    throw new Error("useScores must be used within a ScoreProvider")

  return scoresContext as ScoresContext

}

export { ScoresProvider, useScores }
export type { Action, State }
