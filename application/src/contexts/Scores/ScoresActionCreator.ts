import { IScore } from "../../_definitions/IScore"
import { ScoresActions } from "./ScoresActions"

export const addScore = (payload: IScore) => ({
  type: ScoresActions.ADD,
  payload
})

export const removeScore = (payload: string) => ({
  type: ScoresActions.REMOVE,
  payload
})

export const resetAllScores = () => ({
  type: ScoresActions.RESET_ALL
})

export const updateAllScores = (payload: IScore[]) => ({
  type: ScoresActions.UDPATE_ALL,
  payload
})

export const updateScore = (payload: IScore) => ({
  type: ScoresActions.UPDATE,
  payload
})
