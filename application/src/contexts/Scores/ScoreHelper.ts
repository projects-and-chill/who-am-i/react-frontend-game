import { ITeam } from "../../_definitions/ITeam"
import { IScore } from "../../_definitions/IScore"

class ScoreHelper {

  private static format (team: ITeam) : IScore{
    const { score, id: teamId, guessedWords, color } = team
    const newSore : IScore = {
      teamId,
      color,
      score,
      guessedWords
    }
    return newSore
  }

  static formatOne (team: ITeam){
    return ScoreHelper.format(team)
  }

  static formatMany (teams: ITeam[]) : IScore[] {
    const scores : IScore[] = []
    teams.forEach(team => {
      scores.push(ScoreHelper.format(team))
    })
    return scores
  }

}

export { ScoreHelper }
