import { ITeam } from "../../_definitions/ITeam"
import { TeamsActions } from "./TeamsActions"

export const addTeam = (payload: ITeam) => ({
  type: TeamsActions.ADD,
  payload
})

export const removeTeam = (payload: string) => ({
  type: TeamsActions.REMOVE,
  payload
})

export const resetAllTeams = () => ({
  type: TeamsActions.RESET_ALL
})

export const updateAllTeams = (payload: ITeam[]) => ({
  type: TeamsActions.UDPATE_ALL,
  payload
})

export const updateTeam = (payload: ITeam) => ({
  type: TeamsActions.UPDATE,
  payload
})
