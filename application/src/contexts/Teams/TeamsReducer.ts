import { ITeam } from "../../_definitions/ITeam"
import { TeamsActions } from "./TeamsActions"
import { Action, State } from "./TeamsContext"

type Reducer = (state: State, action: Action) => State

const TeamReducer: Reducer = (prevState, action) => {

  switch (action.type) {

  case TeamsActions.ADD: {

    const newTeam = action.payload as ITeam
    if (prevState.some((team) => team.id === newTeam.id))
      return prevState
    return [...prevState, newTeam]

  }
  case TeamsActions.REMOVE: {

    const removedTeamId = action.payload as string
    return prevState.filter((team) => team.id !== removedTeamId)

  }
  case TeamsActions.UPDATE: {

    const updTeam = action.payload as ITeam
    const prevStateCopy = [...prevState]
    const index = prevState.findIndex((team) => team.id === updTeam.id)
    if (index !== -1)
      prevStateCopy[index] = updTeam
    return prevStateCopy

  }
  case TeamsActions.UDPATE_ALL:
    return action.payload as ITeam[]
  case TeamsActions.RESET_ALL:
    return []
  default:
    throw new Error(`Unhandled action type: ${action.type}`)

  }

}

export { TeamReducer }
