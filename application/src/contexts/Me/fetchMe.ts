import { IPlayer } from "../../_definitions/IPlayer"
import { Dispatch, SetStateAction } from "react"
import { IMe } from "../../_definitions/IMe"
import { IRoom } from "../../_definitions/IRoom"

type Dispatcher = Dispatch<SetStateAction<IMe>> & Dispatch<IMe>
export class MeServices {

  static fetchMe (
    socketId: string,
    data: IPlayer | IPlayer[],
    dispatchMe: Dispatcher,
    room?: IRoom
  ) {

    let myPlayer: IPlayer | undefined

    if (Array.isArray(data))
      myPlayer = data.find((player) => player.id === socketId)
    else if (data.id === socketId)
      myPlayer = data

    if (!myPlayer)
      return

    let roles: Partial<IMe> = {}

    if (room) {
      roles = {
        leader: room.leader === socketId,
        timeMaster: room.timeMaster === socketId
      }
    }

    dispatchMe((prevMe) => {

      return {
        ...prevMe,
        ...myPlayer,
        ...roles
      }

    })

  }

  static amILeader (leaderId: string, socketID: string, dispatchMe: Dispatcher) {

    if (leaderId === socketID)
      dispatchMe((prevState) => ({ ...prevState, leader: true }))

  }

  static amITimeMaster (
    timeMasterId: string,
    socketID: string,
    dispatchMe: Dispatcher
  ) {

    if (timeMasterId === socketID)
      dispatchMe((prevState) => ({ ...prevState, timeMaster: true }))

  }

}
