import React, { createContext, Dispatch, FC, ReactNode, SetStateAction, useContext, useMemo, useState } from "react"
import { IChrono } from "../../_definitions/IChrono"

type IChronoContext = {
  chrono: IChrono
  setChrono: Dispatch<SetStateAction<IChrono>> & Dispatch<IChrono>
}
type Props = { children?: ReactNode }

const ChronoContext = createContext<IChronoContext | null>(null)

const chronoTemplate: IChrono = {
  id: "",
  roomId: "",
  type: "countdown",
  defaultTime: 0,
  state: "pause",
  startAt: 0,
  milliseconds: 0
}

const ChronoProvider: FC<Props> = ({ children }) => {

  const [chrono, setChrono] = useState<IChrono>(chronoTemplate)

  const value: IChronoContext = useMemo(() => ({ chrono, setChrono }), [chrono])

  return <ChronoContext.Provider value={value}>{children}</ChronoContext.Provider>

}

function useChrono () {

  const chronoContext = useContext(ChronoContext)
  if (!chronoContext)

    throw new Error("useChrono must be used within a ChronoProvider")

  return chronoContext as IChronoContext

}

export { chronoTemplate, ChronoProvider, useChrono }
