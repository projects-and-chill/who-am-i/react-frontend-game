import React, { CSSProperties, memo, useEffect } from "react"
import { Card, Col, Form, Input, InputNumber, message, Row, Select } from "antd"
import TimerField from "./TimerField/TimerField"
import EndGameField from "./EndGameField/EndGameField"
import { useSocketIo } from "../../../../../contexts/SocketIo/SocketIoContext"
import { roomEvents } from "../../../../../_definitions/Events"
import { useRoom } from "../../../../../contexts/Room/RoomContext"
import { useMe } from "../../../../../contexts/Me/MeContext"
import useBreakpoint from "antd/es/grid/hooks/useBreakpoint"
import { usePlayers } from "../../../../../contexts/Players/PlayersContext"
import { useRules } from "../../../../../contexts/Rules/RulesContext"
import { IRules } from "../../../../../_definitions/IRules"
import ScoreIncrement from "./ScoreIncrement/SoreIncrement"

const formItemLayout = {}
const wordPerPlayerLimits = {
  min: 1,
  max: 100
}

const { Option } = Select

const Rules = () => {

  const { socket } = useSocketIo()
  const { room } = useRoom()
  const { rules, setRules } = useRules()
  const { players } = usePlayers()
  const [form] = Form.useForm()
  const { me } = useMe()
  const { xl } = useBreakpoint()

  useEffect(() => {

    socket.on(roomEvents.server.RULES_EDITED, (rules: IRules) => {
      setRules(rules)
    })
    return () => {
      socket.off(roomEvents.server.RULES_EDITED)
    }

  }, [setRules, socket])

  useEffect(() => {

    const resetValue = {
      time: {
        type: rules.time[0],
        time: rules.time[1]
      },
      endGame: {
        type: rules.endGame[0],
        qty: rules.endGame[1]
      },
      wordPerPlayers: rules.wordQty,
      timeMaster: players.find(player => player.id === room.timeMaster)?.username || "unknow",
      changeWordCondition: "leader"
    }

    form.setFieldsValue(resetValue)

  }, [form, players, room.timeMaster, rules])

  const emitRuleChange = (rule: Partial<IRules>) => {

    socket.emit(roomEvents.client.EDIT_RULES, ...[rule, room.id])

  }

  const handleWordQtyChange = (newQty: number) => {

    const { max, min } = wordPerPlayerLimits
    if (newQty < wordPerPlayerLimits.min || newQty > wordPerPlayerLimits.max) {
      return message.warning(
        `The amount of words per player must be between ${min} and ${max}`
      )
    }
    setRules((prev) => ({
      ...prev,
      wordQty: newQty
    }))
    emitRuleChange({ wordQty: newQty })

  }

  const colStyle: CSSProperties = {
    gridTemplateColumns: "repeat(auto-fill, minmax(186px, auto))"
  }

  return (
    <Card
      style={{ minWidth: "21rem" }}
      type='inner'
      title='Rules setting'
      bordered={false}
    >
      <Form
        disabled={!me.leader}
        name='customized_form_controls'
        form={form}
        {...formItemLayout}
        layout='inline'
      >
        <Row
          gutter={[0, 24]}
          justify={xl ? "space-around" : "start"}
          style={{ width: "100%" }}
        >
          <Col style={colStyle}>
            <TimerField emitRuleChange={emitRuleChange} name='time' />
            <Form.Item
              style={{ marginTop: "1.5rem", maxWidth: "15.5rem" }}
              label='Time master'
              name='timeMaster'
              required={true}
              rules={[{ required: true, message: "Please input time master!" }]}
            >
              <Input disabled />
            </Form.Item>
          </Col>

          <Col style={colStyle}>

            <Form.Item
              shouldUpdate
              label='Word per players'
              name='wordPerPlayers'
              required={true}
              rules={[
                { required: true, message: "Please enter words per players" }
              ]}
            >
              <InputNumber
                style={{ width: "4rem" }}
                type='number'
                onChange={handleWordQtyChange}
                value={rules.wordQty}
                min={wordPerPlayerLimits.min}
                max={wordPerPlayerLimits.max}
              />
            </Form.Item>

            <Form.Item
              style={{ marginTop: "1.5rem", maxWidth: "15.5rem" }}
              label='Change word'
              name='changeWordCondition'
              required={true}
              rules={[{ required: true, message: "Please input change word conditions !" }]}
            >
              <Select dropdownMatchSelectWidth={false}>
                <Option value="leader">Leader</Option>
                <Option value="never" disabled >Never</Option>
                <Option value="always" disabled>Always</Option>
                <Option value="afterRound" disabled>After x round</Option>
                <Option value="afterSeconds" disabled> After x secondes</Option>
              </Select>
            </Form.Item>
          </Col>

          <Col style={colStyle}>
            <EndGameField emitRuleChange={emitRuleChange} name='endGame' />
            <br/>
            <ScoreIncrement name="scoreIncrement" />
          </Col>

        </Row>

      </Form>
    </Card>
  )

}

export default memo(Rules)
