import { Form, InputNumber, Select } from "antd"
import React, { FC, useState } from "react"
import { IRules } from "../../../../../../_definitions/IRules"
import { Rule } from "rc-field-form/lib/interface"

const { Option } = Select

type EndGameType = IRules["endGame"][0]
type RulesChangeEmitter = (rule: Partial<IRules>) => void

interface EndGameValue {
  qty: number
  type: EndGameType
}

interface Props {
  value?: EndGameValue
  onChange?: (value: EndGameValue) => void
  emitRuleChange: RulesChangeEmitter
}

const endGameLimit = {
  min: 1,
  max: 100
}

const EndGameInput: FC<Props> = ({
  value = {
    qty: endGameLimit.min,
    type: "points"
  },
  onChange,
  emitRuleChange
}) => {

  const [timer, setTimer] = useState<NodeJS.Timeout>()

  const triggerChange = (changedValue: Partial<EndGameValue>): EndGameValue => {

    const newValue: Required<EndGameValue> = {
      ...value,
      ...changedValue
    }
    onChange?.(newValue)
    return newValue

  }

  const onQtyChange = (newQty: number) => {

    if (Number.isNaN(newQty))
      return
    triggerChange({ qty: newQty })

    let updateField = triggerChange({ qty: newQty })

    if (timer)
      clearTimeout(timer)

    const newTimer = setTimeout(() => {

      const { min, max } = endGameLimit
      if (newQty < min)
        updateField = triggerChange({ qty: min })
      else if (newQty > max)
        updateField = triggerChange({ qty: max })

      const { qty, type } = updateField

      emitRuleChange({ endGame: [type, qty] })

    }, 500)

    setTimer(newTimer)

  }

  const onEndGameTypeChange = (type: EndGameType) => {

    triggerChange({ type })
    emitRuleChange({ endGame: [value.type, value.qty] })

  }

  const selectBefore = (
    <Select
      value={value.type}
      style={{ width: "fit-content" }}
      onChange={onEndGameTypeChange}
    >
      <Option value='rounds'>Rounds</Option>
      <Option value='points'>Points</Option>
    </Select>
  )

  return (
    <span>
      <InputNumber
        type='number'
        step={1}
        min={endGameLimit.min}
        value={value.qty}
        addonBefore={selectBefore}
        style={{ width: "10rem" }}
        onChange={onQtyChange}
      />
    </span>
  )

}

type EndGameFieldProps = {
  name: string
  emitRuleChange: RulesChangeEmitter
}

const EndGameField: FC<EndGameFieldProps> = ({ name, emitRuleChange }) => {

  const checkEndGame = (_: Rule, value: Required<EndGameValue>) => {

    const { qty, type } = value
    const { min, max } = endGameLimit

    if (type === "rounds" && (qty < min || qty > max)) {
      return Promise.reject(
        new Error(`Rounds quantity must be between ${min} and ${max}`)
      )
    }
    else if (value.type === "points" && (qty < min || qty > max)) {
      return Promise.reject(
        new Error(`Points quantity must be between ${min} and ${max}`)
      )
    }
    return Promise.resolve()

  }

  return (
    <Form.Item
      name={name}
      label='End Game'
      required={true}
      rules={[{ validator: checkEndGame }]}
    >
      <EndGameInput emitRuleChange={emitRuleChange} />
    </Form.Item>
  )

}

export default EndGameField
