import { Form, Select } from "antd"
import React, { FC, useState } from "react"

const { Option } = Select
const incrementMethodData = ["manual", "automatic"]
const incrementByData = ["points", "time"]

const ScoreIncrement: FC<{name: string}> = ({ name }) => {

  const [incMethod, setIncMethod] = useState(incrementMethodData[1])
  const [incBy, setIncBy] = useState(incrementByData[0])

  const handleIncrementMethodChange = (value: string) => {
    setIncMethod(value)
  }

  const onSecondCityChange = (value : string) => {
    setIncBy(value)
  }

  return (

    <Form.Item
      label='Score increment'
      name={name}
      required={true}
      rules={[
        { required: true, message: "Please select score increment method" }
      ]}
    >
      <div style={{ display: "flex", flexDirection: "column" }}>
        <Select value={incMethod} style={{ width: 120 }} onChange={handleIncrementMethodChange}>
          {incrementMethodData.map(method => (
            <Option disabled={method === "manual"} key={method}>{method}</Option>
          ))}
        </Select>
        {incMethod === "automatic" &&
          <Select style={{ width: 120 }} value={incBy} onChange={onSecondCityChange}>
            {incrementByData.map(by => (
              <Option disabled={by === "time"} key={by}>{by}</Option>
            ))}
          </Select>}
      </div>

    </Form.Item>
  )
}

export default ScoreIncrement
