import { useSocketIo } from "../../../../../contexts/SocketIo/SocketIoContext"
import { usePlayers } from "../../../../../contexts/Players/PlayersContext"
import { useTeams } from "../../../../../contexts/Teams/TeamsContext"
import React, { memo, useCallback, useEffect } from "react"
import { playerEvents, teamEvents } from "../../../../../_definitions/Events"
import { Button, Card, Col, message, Row } from "antd"
import { PlusOutlined, TeamOutlined } from "@ant-design/icons"
import { ITeam } from "../../../../../_definitions/ITeam"
import { addTeam } from "../../../../../contexts/Teams/TeamsActionCreator"
import PlayersList from "./PlayersList/PlayersList"
import { useMe } from "../../../../../contexts/Me/MeContext"
import { IScore } from "../../../../../_definitions/IScore"
import { useScores } from "../../../../../contexts/Scores/ScoresContext"
import { addScore } from "../../../../../contexts/Scores/ScoresActionCreator"
import { ScoreHelper } from "../../../../../contexts/Scores/ScoreHelper"
import { IPlayer } from "../../../../../_definitions/IPlayer"
import { MeServices } from "../../../../../contexts/Me/fetchMe"
import { updatePlayer } from "../../../../../contexts/Players/PlayersActionCreator"

function Teams () {

  const { socket } = useSocketIo()
  const { teams, setTeams } = useTeams()
  const { setScores } = useScores()
  const { players, setPlayers } = usePlayers()
  const { me, setMe } = useMe()

  const handleSwitchTeam = useCallback(
    (newTeam: string, roomId: string) => {

      if (me.teamId === newTeam)
        return message.warning("You are already in this team")
      socket.emit(playerEvents.client.SWITCH_TEAM, ...[newTeam, roomId])

    },
    [me.teamId, socket]
  )

  const handleCreateTeam = useCallback(() => {

    if (!me.id)
      return
    const count = players.filter((player) => player.teamId === me.teamId).length
    if (count < 2)
      return message.warning("Hey calm down !! You are alone in your team !")

    socket.emit(teamEvents.client.CREATE, ...[me.roomId])

  }, [me.id, me.roomId, me.teamId, players, socket])

  useEffect(() => {

    socket
      .on(teamEvents.server.CREATED, (team: ITeam) => {
        setTeams(addTeam(team))
        const score : IScore = ScoreHelper.formatOne(team)
        setScores(addScore(score))
      })
      .on(playerEvents.server.TEAM_SWITCHED, (player: IPlayer) => {
        MeServices.fetchMe(socket.id, player, setMe)
        setPlayers(updatePlayer(player))
      })

    return () => {
      socket
        .off(playerEvents.server.TEAM_SWITCHED)
        .off(teamEvents.server.CREATED)
    }

  }, [setMe, setPlayers, setScores, setTeams, socket])

  return (
    <>
      <Row justify='start' align='top'>
        {teams.map((team) => (
          <Col
            key={team.id}
            span={4}
            style={{ marginRight: "1rem", minWidth: "12rem" }}
          >
            <Card
              style={{ marginTop: 16 }}
              headStyle={{
                color: team.color
              }}
              type='inner'
              extra={
                <Button //SWITCH TEAM
                  style={{ borderColor: team.color }}
                  disabled={me.teamId === team.id}
                  type='default'
                  shape='round'
                  onClick={() => handleSwitchTeam(team.id, team.roomId)}
                  icon={<TeamOutlined style={{ color: team.color }} />}
                  size='middle'
                />
              }
              title={`Team ${team.id}`}
              bordered={false}
            >
              <PlayersList teamId={team.id} />
            </Card>
          </Col>
        ))}

        <Col style={{ alignSelf: "end" }}>
          <Button //CREATE TEAM
            type='default'
            icon={<PlusOutlined />}
            size='large'
            onClick={handleCreateTeam}
          />
        </Col>
      </Row>
    </>
  )

}

export default memo(Teams)
