import React, { FC, memo, useCallback } from "react"
import { Avatar, List } from "antd"
import { IPlayer } from "../../../../../../_definitions/IPlayer"
import { usePlayers } from "../../../../../../contexts/Players/PlayersContext"
import { CrownOutlined } from "@ant-design/icons"
import { useRoom } from "../../../../../../contexts/Room/RoomContext"

type PlayerProps = {
  teamId: string
}

const PlayersList: FC<PlayerProps> = ({ teamId }) => {
  const { players } = usePlayers()
  const { room } = useRoom()

  const roomLeader = useCallback(
    (playerId: string) => {
      if (playerId === room.leader)
        return <CrownOutlined style={{ color: "#f0aa13" }} />
      return " "
    },
    [room.leader]
  )

  return (
    <List
      itemLayout='horizontal'
      dataSource={players.filter((player) => player.teamId === teamId)}
      renderItem={(player: IPlayer) => (
        <List.Item style={{ padding: "0" }} extra={roomLeader(player.id)}>
          <Avatar
            size={{ xs: 24, sm: 32, md: 40, lg: 50, xl: 65, xxl: 75 }}
            src={player.avatar}
          />
          <div>{player.username}</div>
        </List.Item>
      )}
    />
  )

}

export default memo(PlayersList)
