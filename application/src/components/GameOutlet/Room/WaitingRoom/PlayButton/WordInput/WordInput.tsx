import { Divider, Progress, Select, Spin, Tag } from "antd"
import React, { useCallback, useMemo, useState } from "react"
import { useSocketIo } from "../../../../../../contexts/SocketIo/SocketIoContext"
import { wordEvent } from "../../../../../../_definitions/Events"
import { useRules } from "../../../../../../contexts/Rules/RulesContext"
import { useMe } from "../../../../../../contexts/Me/MeContext"

const WordInput: React.FC = () => {

  const [wordSet, setWordSet] = useState<Set<string>>(() => new Set())
  const { me } = useMe()
  const { rules } = useRules()
  const { socket } = useSocketIo()

  const handleSelect = useCallback(
    (value: string) => {

      if (wordSet.size === rules.wordQty)
        return
      if (wordSet.has(value))
        return
      socket.emit(
        wordEvent.client.ADD,
        ...[value, me.roomId],
        (wrd: string) => {

          setWordSet((prevSet) => new Set(prevSet).add(wrd))

        }
      )

    },
    [me.roomId, rules.wordQty, socket, wordSet]
  )

  const handleDeselect = useCallback(
    (value: string) => {

      if (!wordSet.has(value))
        return
      setWordSet((prevSet) => {

        const next = new Set(prevSet)
        next.delete(value)
        return next

      })
      socket.emit(wordEvent.client.REMOVE, ...[value, me.roomId])

    },
    [me.roomId, socket, wordSet]
  )

  const wordQty: number = useMemo(() => rules.wordQty, [rules.wordQty])

  const progress: number = useMemo(() => {

    return Math.floor((wordSet.size / wordQty) * 100)

  }, [wordQty, wordSet.size])

  return (
    <div>
      <Divider orientation='left'>
        Enter <Tag color='blue'>{wordQty}</Tag> words
      </Divider>

      <div>
        <Progress trailColor={"grey"} percent={progress} steps={wordQty} />
      </div>

      <Select
        mode={"tags"}
        open={false}
        style={{ width: "100%", border: " 1px solid #4e8cc7" }}
        size='large'
        maxTagTextLength={18}
        allowClear={true}
        tokenSeparators={[","]}
        placeholder='Enter your words'
        value={Array.from(wordSet)}
        onSelect={handleSelect}
        onDeselect={handleDeselect}
        notFoundContent={null}
      />
      {progress === 100 && (
        <div style={{ marginTop: "1rem" }}>
          <Spin /> Waiting other players
        </div>
      )}
    </div>
  )

}

export default WordInput
