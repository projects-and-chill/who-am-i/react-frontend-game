import React, { JSXElementConstructor, useEffect } from "react"
import { useRoom } from "../../../contexts/Room/RoomContext"
import Round from "./Round/Round"
import WaitingRoom from "./WaitingRoom/WaitingRoom"
import { useNavigate } from "react-router-dom"
import { useSocketIo } from "../../../contexts/SocketIo/SocketIoContext"
import { playerEvents, roomEvents, teamEvents } from "../../../_definitions/Events"
import { GameStep, IRoom } from "../../../_definitions/IRoom"
import { MeServices } from "../../../contexts/Me/fetchMe"
import { useMe } from "../../../contexts/Me/MeContext"
import { addPlayer, removePlayer } from "../../../contexts/Players/PlayersActionCreator"
import { ImportOutlined, UserAddOutlined } from "@ant-design/icons"
import { usePlayers } from "../../../contexts/Players/PlayersContext"
import { IPlayer } from "../../../_definitions/IPlayer"
import { notification } from "antd"
import { useTeams } from "../../../contexts/Teams/TeamsContext"
import { useScores } from "../../../contexts/Scores/ScoresContext"
import { removeTeam } from "../../../contexts/Teams/TeamsActionCreator"
import { removeScore } from "../../../contexts/Scores/ScoresActionCreator"
import { debug as dbg } from "debug"
import PostGame from "./PostGame/PostGame"

const debug = dbg("ROOM")

const Room = () => {

  const { room, setRoom } = useRoom()
  const { setMe } = useMe()
  const { setTeams } = useTeams()
  const { setScores } = useScores()
  const { setPlayers } = usePlayers()
  const navigate = useNavigate()
  const { socket } = useSocketIo()

  useEffect(() => {

    debug("%s", "UF")
    if (!room.id)
      navigate("game/lobby", { replace: false })

    socket
      .on(roomEvents.server.LEADER_CHANGE, (room: IRoom) => {
        MeServices.amILeader(room.leader, socket.id, setMe)
        setRoom(room)
      })
      .on(playerEvents.server.LEAVED, (username: string, playerId: string) => {
        setPlayers(removePlayer(playerId))
        playerNotification(username, "leave", "#f78e25", ImportOutlined)
      })
      .on(playerEvents.server.JOINED, (username: string, player: IPlayer) => {
        setPlayers(addPlayer(player))
        playerNotification(player.username, "join", "#108ee9", UserAddOutlined)
      })
      .on(teamEvents.server.DELETED, (teamId: string) => {
        setTeams(removeTeam(teamId))
        setScores(removeScore(teamId))
      })

    return () => {
      debug("%s", "-----LEAVE ROOM  & DISCONNECT-----", room.id)
      socket
        .off(roomEvents.server.LEADER_CHANGE)
        .off(playerEvents.server.JOINED)
        .off(playerEvents.server.LEAVED)
        .off(teamEvents.server.DELETED)
        .disconnect()
    }

  },
  [navigate, room.id, setMe, setPlayers, setRoom, setScores, setTeams, socket])

  const playerNotification = (
    username: string,
    action: string,
    color: string,
    Icon: JSXElementConstructor<{ style: Record<string, unknown> }>
  ) => {
    notification.open({
      message: (<div>Player <span style={{ color, fontWeight: "bold" }}>{username}</span>{" "}{action} the room !</div>),
      icon: <Icon style={{ color }} />
    })
  }

  return (
    <div>
      {!room.started && (room.gameStep === GameStep.preGame) && <WaitingRoom />}
      {room.started && (room.gameStep !== GameStep.postGame) && <Round />}
      {room.started && (room.gameStep === GameStep.postGame) && <PostGame />}
    </div>
  )
}

export default Room
