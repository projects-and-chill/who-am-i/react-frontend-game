import React, { memo } from "react"
import { Button } from "antd"
import { useNavigate } from "react-router-dom"
import ScoreTable from "../Round/Structure/ScoreTable/ScoreTable"
import MulticolorTitle from "../../../Structure/MulticoloreTitle/MulticolorTitle"

const PostGame = memo(() => {

  const navigate = useNavigate()

  const handleLeave = () =>{
    navigate("game/lobby", { replace: false })
  }

  return (
    <section>
      <div style={{ marginBottom: "2rem" }}>
        <MulticolorTitle content={["END", "GAME"]} />
      </div>
      <div style={{ marginBottom: "2rem" }}>
        <ScoreTable />
      </div>

      <div>
        <Button>Replay</Button>
        <Button onClick={handleLeave} danger>Leave</Button>
      </div>

    </section>
  )

})
PostGame.displayName = "PostGame"

export default PostGame
