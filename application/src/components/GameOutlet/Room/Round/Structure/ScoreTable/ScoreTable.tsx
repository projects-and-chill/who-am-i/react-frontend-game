import React, { FC, useEffect } from "react"
import { useScores } from "../../../../../../contexts/Scores/ScoresContext"
import { IScore } from "../../../../../../_definitions/IScore"
import { Button, Card, Col, message, Row } from "antd"
import FlipNumber from "../../../../../Structure/FlipNumber/FlipNumber"
import { MinusOutlined, PlusOutlined } from "@ant-design/icons"
import { useSocketIo } from "../../../../../../contexts/SocketIo/SocketIoContext"
import { teamEvents } from "../../../../../../_definitions/Events"
import { ITeam } from "../../../../../../_definitions/ITeam"
import { updateScore } from "../../../../../../contexts/Scores/ScoresActionCreator"
import { useMe } from "../../../../../../contexts/Me/MeContext"
import { ScoreHelper } from "../../../../../../contexts/Scores/ScoreHelper"

const ScoreTable : FC = () => {

  const { scores, setScores } = useScores()
  const { me } = useMe()
  const { socket } = useSocketIo()

  useEffect(()=> {
    socket.on(teamEvents.server.SCORE_UPDATED, (team: ITeam)=>{
      const newSore : IScore = ScoreHelper.formatOne(team)
      setScores(updateScore(newSore))
    })

    return ()=> {
      socket.off(teamEvents.server.SCORE_UPDATED)
    }
  })

  const handleScore = (teamId: string, action: "increase" | "decrease") => {
    if (me.teamId !== teamId && !me.leader)
      return message.warning("Only leader or team owners can update this score")
    let score : number = (scores.find(score => score.teamId === teamId) as IScore).score
    if (action === "increase")
      score += 1
    else if (action === "decrease")
      score += -1

    socket.emit(teamEvents.client.UPDATE_SCORE, ...[score, teamId, me.roomId])
  }

  return (
    <div style={{
      padding: "30px",
      backgroundColor: "#dedede"

    }} className="site-card-wrapper">
      <Row
        style={{
          justifyContent: "space-evenly"
        }}
        gutter={[30, 30]}>

        {scores.map(score => (
          <div key={score.teamId} style={{ minWidth: "11rem" }}>
            <Col>
              <Card title={(<span style={{ textTransform: "capitalize", color: score.color }}>{score.teamId}</span>)} bordered={false}>
                <FlipNumber number={score.score} color={score.color} fontSize={45} />
              </Card>
            </Col>

            <div style={{ display: "flex", justifyContent: "space-evenly", marginTop: "0.5rem" }}>
              <Button onClick={()=> handleScore(score.teamId, "decrease") } icon={<MinusOutlined />} />
              <Button onClick={()=> handleScore(score.teamId, "increase")} icon={<PlusOutlined />} />
            </div>
          </div>
        ))}

      </Row>
    </div>
  )
}

export default ScoreTable
