import React, { FC, memo, useCallback, useMemo } from "react"
import { Avatar, List, Tag } from "antd"
import { usePlayers } from "../../../../../../../contexts/Players/PlayersContext"
import { useRoom } from "../../../../../../../contexts/Room/RoomContext"
import { CrownOutlined } from "@ant-design/icons"
import { ITeam } from "../../../../../../../_definitions/ITeam"

type PlayerProps = {
  teamId: string
  teamTurn: ITeam|undefined,
  teamsColors: Record<string, string>
}

const NestedList: FC<PlayerProps> = ({
  teamTurn,
  teamId,
  teamsColors
}) => {

  const { players } = usePlayers()
  const { room } = useRoom()

  const roomLeader = useCallback(
    (playerId: string) => {
      if (playerId === room.leader)
        return <CrownOutlined style={{ color: "#f0aa13" }} />
      return ""
    },
    [room.leader]
  )

  const filteredPlayers = useMemo(
    () => players.filter(player => player.teamId === teamId),
    [players, teamId]
  )

  return (
    <List
      size="small"
      dataSource={filteredPlayers}
      renderItem={player =>(
        <List.Item >
          <div>
            <Avatar src={player.avatar} />
            <span style={{ marginLeft: "1rem", color: teamsColors[player.teamId] }} >
              {player.username}
            </span>

            <span style={{ marginLeft: "1rem", marginRight: "1rem" }} >
              {roomLeader(player.id)}
            </span>

          </div>

          <div>
            {
              (player.blinded && (player.teamId === teamTurn?.id)) && <Tag color="geekblue">Blinded</Tag>
            }
          </div>
        </List.Item>
      )} />
  )

}

export default memo(NestedList)
