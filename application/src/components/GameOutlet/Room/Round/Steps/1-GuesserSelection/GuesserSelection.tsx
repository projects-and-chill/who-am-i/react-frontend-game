import React, { FC, memo, useEffect, useMemo, useState } from "react"
import { Avatar, Button, message, Segmented } from "antd"
import { usePlayers } from "../../../../../../contexts/Players/PlayersContext"
import { useRoom } from "../../../../../../contexts/Room/RoomContext"
import { IPlayer } from "../../../../../../_definitions/IPlayer"
import { useSocketIo } from "../../../../../../contexts/SocketIo/SocketIoContext"
import { playerEvents, roomEvents } from "../../../../../../_definitions/Events"
import { updatePlayer } from "../../../../../../contexts/Players/PlayersActionCreator"
import { CheckOutlined } from "@ant-design/icons"
import { useMe } from "../../../../../../contexts/Me/MeContext"
import { MeServices } from "../../../../../../contexts/Me/fetchMe"
import MulticolorTitle from "../../../../../Structure/MulticoloreTitle/MulticolorTitle"
import { ITeam } from "../../../../../../_definitions/ITeam"

type Props = {
  myTurn : boolean,
  teamTurn : ITeam
}

const GuesserSelection : FC<Props> = ({ myTurn, teamTurn }) => {

  const { players, setPlayers } = usePlayers()
  const { room } = useRoom()
  const { socket } = useSocketIo()
  const { me, setMe } = useMe()

  const playersTurn : IPlayer[] = useMemo(()=> players.filter(player => {
    return player.teamId === room.teamTurn
  }), [players, room.teamTurn])

  const [guesser, setGuesser] = useState(playersTurn.find(player => player.blinded)?.id || "")

  useEffect(()=>{
    socket.on(playerEvents.server.BLINDED, (blinded:IPlayer, unblinded: IPlayer|undefined)=>{
      setGuesser(blinded.id)

      if (unblinded){
        setPlayers(updatePlayer(unblinded))
        MeServices.fetchMe(socket.id, unblinded, setMe)
      }

      setPlayers(updatePlayer(blinded))
      MeServices.fetchMe(socket.id, blinded, setMe)
    })

    return ()=> {
      socket.off(playerEvents.server.BLINDED)
    }
  }, [setMe, setPlayers, socket])

  const handleBlind = (playerId: unknown) => {
    const playerBlinded : IPlayer | undefined = playersTurn.find(player => player.id === playerId)

    if (!playerBlinded)
      return message.warning("Please select correct player")

    else if (playerBlinded.blinded)
      return message.warning("Player already blinded")

    else if (!myTurn)
      return message.warning("It's not your turn")

    socket.emit(playerEvents.client.BLIND, ...[playerId, room.id])
  }

  const validateGuesser = () =>{
    if (!guesser)
      message.warning("You should select guesser !")

    socket.emit(roomEvents.client.NEXT_STEP, room.id)
  }

  return (
    <section>
      <MulticolorTitle content={["Who", "will be", "blinded ?"]} />
      <br/>
      <Segmented
        style={{ border: "solid 3px " + teamTurn?.color }}
        disabled={room.teamTurn !== me.teamId}
        onChange={handleBlind}
        value={guesser}
        options={playersTurn.map(player => ({
          label: (
            <div style={{ padding: 4 }}>
              <Avatar size={{ xs: 45, sm: 70, md: 110, lg: 150, xl: 200, xxl: 250 }} src={player.avatar} />
              <p style={{ color: teamTurn?.color, textTransform: "uppercase", fontWeight: "bold" }}>{player.username}</p>
            </div>
          ),
          value: player.id
        }))}
      />
      <div style={{ marginTop: "1rem" }}>
        <Button
          loading={!guesser}
          disabled={!myTurn} onClick={validateGuesser} size="large" icon={<CheckOutlined />} type="primary" > Continue </Button>
      </div>

    </section>
  )

}

export default memo(GuesserSelection)
