import React, { FC, memo, useEffect, useMemo, useState } from "react"
import { useTeams } from "../../../../../../contexts/Teams/TeamsContext"
import { useSocketIo } from "../../../../../../contexts/SocketIo/SocketIoContext"
import { teamEvents, wordEvent } from "../../../../../../_definitions/Events"
import { ITeam } from "../../../../../../_definitions/ITeam"
import { IWord } from "../../../../../../_definitions/IWord"
import { updateTeam } from "../../../../../../contexts/Teams/TeamsActionCreator"
import { Spin } from "antd"
import { useMe } from "../../../../../../contexts/Me/MeContext"
import { usePlayers } from "../../../../../../contexts/Players/PlayersContext"
import WordVoting from "./WordVoting/WordVoting"
import WordDisplay from "./WordDisplay/WordDisplay"
import WordKeeping from "./WordKeeping/WordKeeping"

type WordValidationProps = {
  myTurn : boolean,
  teamTurn: ITeam
}

const WordValidation : FC<WordValidationProps> = ({ myTurn, teamTurn }) => {

  const { socket } = useSocketIo()
  const [word, setWord] = useState<IWord>()
  const { setPlayers } = usePlayers()
  const { setTeams } = useTeams()
  const { me } = useMe()

  const canSeeWord : boolean = useMemo(() => !(me.blinded && myTurn),
    [me.blinded, myTurn])

  useEffect(()=>{

    socket
      .on(wordEvent.server.VALIDATED, (word: IWord)=> {
        setWord(word)
      })
      .on(wordEvent.server.CHANGED, (word: IWord, team : ITeam)=> {
        setWord(word)
        setTeams(updateTeam(team))
      })

    const getWord = (wordId: string) => socket.emit(wordEvent.client.GET, ...[wordId, me.roomId], (word: IWord)=>{
      setWord(word)
    })

    if (teamTurn.wordId) {
      getWord(teamTurn.wordId)
    }
    else {
      socket.emit(teamEvents.client.GET, ...[teamTurn.id, teamTurn.roomId], (team: ITeam)=>{
        setTeams(updateTeam(team))
        if (team.wordId)
          getWord(team.wordId)
      })
    }

    return ()=> {
      socket
        .off(wordEvent.server.VALIDATED)
        .off(wordEvent.server.CHANGED)
    }

  }, [me.roomId, setPlayers, setTeams, socket, teamTurn])

  return (
    <section>
      <Spin spinning={(!word || word?.validate === -1) && canSeeWord} size="large" />

      <WordDisplay word={word} myTurn={myTurn} />

      <br/>

      {word &&
        <>
          {word.validate === 0 && <WordVoting teamTurn={teamTurn} myTurn={myTurn} /> }
          {word.validate === 1 && <WordKeeping myTurn={myTurn} /> }
        </>
      }

    </section>
  )
}

export default memo(WordValidation)
