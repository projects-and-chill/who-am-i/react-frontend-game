import React, { FC, useEffect, useState } from "react"
import { useSocketIo } from "../../../../../../contexts/SocketIo/SocketIoContext"
import { roomEvents, teamEvents } from "../../../../../../_definitions/Events"
import { useMe } from "../../../../../../contexts/Me/MeContext"
import { Button } from "antd"
import { GameStep } from "../../../../../../_definitions/IRoom"
import { Content } from "antd/es/layout/layout"
import MulticolorTitle from "../../../../../Structure/MulticoloreTitle/MulticolorTitle"

type WordValidationProps = {
  myTurn : boolean
}

const ScoreUpdating : FC<WordValidationProps> = ({ myTurn }) => {

  const { socket } = useSocketIo()
  const { me } = useMe()
  const [image, setImage] = useState<string>("")

  useEffect(()=>{

    const links = [
      "https://media.giphy.com/media/l3q2NR6S4uOC63ET6/giphy.gif",
      "https://media.giphy.com/media/tCQv89VrByDIs/giphy.gif",
      "https://media.giphy.com/media/2vqCeZa9FDGGe8moT9/giphy.gif",
      "https://media.giphy.com/media/QVybEy0yq80MxL34NV/giphy.gif"

    ]
    const link = links[Math.floor(Math.random() * links.length)] || links[0]
    setImage(link)
  }, [])

  const handleTurnEnd = ()=> {
    socket.emit(teamEvents.client.TURN_END, ...[me.teamId, me.roomId])
  }

  const handleNewWord = ()=> {
    socket.emit(roomEvents.client.GO_TO_STEP, ...[GameStep.guesserSelection, me.roomId])
  }

  return (
    <section>
      <MulticolorTitle content={["What", "do we do", "now ?"]} />
      <br/>
      <Content style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
      }}>
        <div>
          <img alt={""} src={image} style={{ maxHeight: "18rem", width: "100%" }} />
        </div>
        <div>
          <br/>
          <Button size="large" disabled={!myTurn} onClick={handleNewWord} > NEW WORD</Button>
          <Button size="large" danger disabled={!myTurn} onClick={handleTurnEnd}> TURN END</Button>
        </div>
      </Content>
    </section>

  )
}

export default ScoreUpdating
