import React, { useEffect, useMemo } from "react"
import { useRoom } from "../../../../../contexts/Room/RoomContext"
import WordSelection from "./2-wordValidation/WordValidation"
import { useSocketIo } from "../../../../../contexts/SocketIo/SocketIoContext"
import { roomEvents, teamEvents } from "../../../../../_definitions/Events"
import { GameStep, IRoom } from "../../../../../_definitions/IRoom"
import { ITeam } from "../../../../../_definitions/ITeam"
import { useTeams } from "../../../../../contexts/Teams/TeamsContext"
import { updateAllTeams, updateTeam } from "../../../../../contexts/Teams/TeamsActionCreator"
import GuesserSelection from "./1-GuesserSelection/GuesserSelection"
import ScoreUpdating from "./4-ScoreUpdating/ScoreUpdating"
import WordGuessing from "./3-WordGuessing/WordGuessing"
import { Spin } from "antd"
import { useMe } from "../../../../../contexts/Me/MeContext"

const Steps = () => {

  const { socket } = useSocketIo()
  const { room, setRoom } = useRoom()
  const { setTeams, teams } = useTeams()
  const { me } = useMe()

  const teamTurn = useMemo<ITeam|undefined>(()=>{
    return teams.find(team =>team.id === room.teamTurn) as ITeam
  }, [teams, room.teamTurn])

  const myTurn : boolean = useMemo(()=> {
    return room.teamTurn === me.teamId
  }, [me.teamId, room.teamTurn])

  useEffect(() => {

    socket
      .on(roomEvents.server.NEXT_ROUND, (room: IRoom, teams : ITeam[]) => {
        setRoom(room)
        setTeams(updateAllTeams(teams))
      })
      .on(roomEvents.server.NEXT_TEAM, (room: IRoom) => {
        setRoom(room)
      })
      .on(teamEvents.server.TURN_ENDED, (team:ITeam, room : IRoom)=> {
        setRoom(room)
        setTeams(updateTeam(team))
      })
      .on(roomEvents.server.STEP_CHANGED, (room : IRoom)=> {
        setRoom(room)
      })

    return () => {
      socket
        .off(roomEvents.server.NEXT_ROUND)
        .off(roomEvents.server.NEXT_TEAM)
        .off(roomEvents.server.STEP_CHANGED)
        .off(teamEvents.server.TURN_ENDED)
    }

  }, [setRoom, setTeams, socket])

  return (
    <div>
      <Spin size="large" spinning={!room.teamTurn} tip="Loading...">
        {teamTurn &&
          <div>
            {room.gameStep === GameStep.guesserSelection && <GuesserSelection myTurn={myTurn} teamTurn={teamTurn} />}
            {room.gameStep === GameStep.wordSelection && <WordSelection myTurn={myTurn} teamTurn={teamTurn} />}
            {room.gameStep === GameStep.wordGuessing && <WordGuessing myTurn={myTurn} teamTurn={teamTurn} />}
            {room.gameStep === GameStep.scoreUpdating && <ScoreUpdating myTurn={myTurn} />}
          </div>
        }
      </Spin>
    </div>
  )

}

export default Steps
