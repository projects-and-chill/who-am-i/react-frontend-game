import React, { FC, useCallback, useEffect, useState } from "react"
import { Button, Progress } from "antd"
import { MinusOutlined, PlusOutlined } from "@ant-design/icons"
import { useChrono } from "../../../../../../../../contexts/Chrono/ChronoContext"
import { useSocketIo } from "../../../../../../../../contexts/SocketIo/SocketIoContext"
import { chronoEvents } from "../../../../../../../../_definitions/Events"
import { IChrono } from "../../../../../../../../_definitions/IChrono"
import { useRoom } from "../../../../../../../../contexts/Room/RoomContext"
import styleModule from "./CountDownTimer.module.css"

const getMilliseconds = (chrono: IChrono) : number => chrono.defaultTime - chrono.milliseconds

const round = (value: number, precision: number = 0) => {
  const multiplier = Math.pow(10, precision)
  return Math.round(value * multiplier) / multiplier
}

const CountDownTimer : FC = () => {

  const { chrono, setChrono } = useChrono()
  const { room } = useRoom()
  const { socket } = useSocketIo()

  const [_, setTimer] = useState<NodeJS.Timer|null>(null) //eslint-disable-line @typescript-eslint/no-unused-vars
  const [milliseconds, setMilliseconds] = useState<number>(chrono.defaultTime)

  const clearTimer = useCallback(() => setTimer(prevTimer=> {
    if (prevTimer)
      clearInterval(prevTimer)
    return null
  }), [])

  useEffect(()=>{
    socket
      .on(chronoEvents.server.RESETED, (chrono: IChrono) => {
        clearTimer()
        setChrono(chrono)
        setMilliseconds(getMilliseconds(chrono))
      })
      .on(chronoEvents.server.INCREMENTED, (chrono: IChrono) => {
        setChrono(chrono)
        setMilliseconds(getMilliseconds(chrono))
      })
      .on(chronoEvents.server.PAUSE, (chrono: IChrono) => {
        clearTimer()
        setChrono(chrono)
        setMilliseconds(getMilliseconds(chrono))
      })
      .on(chronoEvents.server.PLAY, (chrono: IChrono) => {
        setChrono(chrono)
        setMilliseconds(getMilliseconds(chrono))
        const msDecrease = 100
        const newTimer = setInterval(()=>{
          setMilliseconds(prevTime => prevTime - msDecrease)
        }, msDecrease)
        setTimer(newTimer)
      })

    return ()=> {
      clearTimer()
      socket
        .off(chronoEvents.server.RESETED)
        .off(chronoEvents.server.INCREMENTED)
        .off(chronoEvents.server.PLAY)
        .off(chronoEvents.server.PAUSE)
    }
  }, [clearTimer, setChrono, socket])

  const handleIncrement = (ms: number) => {
    socket.emit(chronoEvents.client.INCREMENT, ...[ms, chrono.id, room.id])
  }

  const handleToggleState = () =>{
    const state : IChrono["state"] = chrono.state === "pause" ? "play" : "pause"
    socket.emit(chronoEvents.client.TOGGLE_STATE, ...[state, chrono.id, room.id])
  }

  const handleReset = () =>{
    socket.emit(chronoEvents.client.RESET, ...[chrono.id, room.id])
  }

  const millisecondsToPercents = () : number => {
    return milliseconds * 100 / chrono.defaultTime
  }

  return (
    <section>

      <div
        style={{
          display: "flex",
          alignItems: "center"
        }}
      >

        <div onClick={handleToggleState}>
          <Progress
            className={styleModule.countDown}
            style={{ marginBottom: "1rem", marginRight: "1rem", cursor: "pointer" }}
            type="circle"
            width={250}
            status={"active"}
            strokeColor={{
              "0%": "#108ee9",
              "100%": "#87d068"
            }}
            format={() => `${round(milliseconds / 1000, 1).toFixed(1) }s` }
            percent={millisecondsToPercents()}
            trailColor={"grey"}
          />
        </div>

        <div style={{ display: "flex", flexDirection: "column", minWidth: "5rem" }}>

          <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "1rem" }}>
            <Button onClick={()=> handleIncrement(-5000)} icon={<MinusOutlined />} />
            <Button onClick={()=> handleIncrement(5000)} icon={<PlusOutlined />} />
          </div>

          <div >
            <Button type="dashed" danger style={{ width: "100%" }} onClick={handleReset}> Reset </Button>
          </div>
        </div>

      </div>
    </section>
  )
}

export default CountDownTimer
