import React, { FC, useCallback, useEffect, useState } from "react"
import { Button, Progress } from "antd"
import { useChrono } from "../../../../../../../../contexts/Chrono/ChronoContext"
import { useSocketIo } from "../../../../../../../../contexts/SocketIo/SocketIoContext"
import { chronoEvents } from "../../../../../../../../_definitions/Events"
import { IChrono } from "../../../../../../../../_definitions/IChrono"
import { useRoom } from "../../../../../../../../contexts/Room/RoomContext"
import styleModule from "./StopWatchTimer.module.css"

const getMilliseconds = (chrono: IChrono) : number => chrono.milliseconds

const round = (value: number, precision: number = 0) => {
  const multiplier = Math.pow(10, precision)
  return Math.round(value * multiplier) / multiplier
}

const StopWatchTimer : FC = () => {

  const { chrono, setChrono } = useChrono()
  const { room } = useRoom()
  const { socket } = useSocketIo()

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [_, setTimer] = useState<NodeJS.Timer|null>(null)
  const [milliseconds, setMilliseconds] = useState<number>(0)

  const clearTimer = useCallback(() => setTimer(prevTimer=> {
    if (prevTimer)
      clearInterval(prevTimer)
    return null
  }), [])

  useEffect(()=>{
    socket
      .on(chronoEvents.server.RESETED, (chrono: IChrono) => {
        clearTimer()
        setChrono(chrono)
        setMilliseconds(getMilliseconds(chrono))
      })
      .on(chronoEvents.server.PAUSE, (chrono: IChrono) => {
        clearTimer()
        setChrono(chrono)
        setMilliseconds(getMilliseconds(chrono))
      })
      .on(chronoEvents.server.PLAY, (chrono: IChrono) => {
        setChrono(chrono)
        setMilliseconds(getMilliseconds(chrono))
        const msDecrease = 100
        const newTimer = setInterval(()=>{
          setMilliseconds(prevTime => prevTime + msDecrease)
        }, msDecrease)
        setTimer(newTimer)
      })

    return ()=> {
      clearTimer()
      socket
        .off(chronoEvents.server.RESETED)
        .off(chronoEvents.server.PLAY)
        .off(chronoEvents.server.PAUSE)
    }
  }, [clearTimer, setChrono, socket])

  const handleToggleState = () =>{
    const state : IChrono["state"] = chrono.state === "pause" ? "play" : "pause"
    socket.emit(chronoEvents.client.TOGGLE_STATE, ...[state, chrono.id, room.id])
  }

  const handleReset = () =>{
    socket.emit(chronoEvents.client.RESET, ...[chrono.id, room.id])
  }

  return (
    <section>

      <div
        style={{
          display: "flex",
          alignItems: "center"
        }}
      >

        <div onClick={handleToggleState}>
          <Progress
            className={styleModule.countDown}
            style={{ marginBottom: "1rem", marginRight: "1rem", cursor: "pointer" }}
            type="circle"
            width={250}
            status={"active"}
            strokeColor={{
              "0%": "#108ee9",
              "100%": "#87d068"
            }}
            format={() => `${round(milliseconds / 1000, 1).toFixed(1) }s` }
            percent={chrono.state === "pause" ? 0 : 100}
            trailColor={"grey"}
          />
        </div>

        <div style={{ minWidth: "5rem" }}>
          <Button type="dashed" danger style={{ width: "100%" }} onClick={handleReset}> Reset </Button>
        </div>

      </div>
    </section>
  )
}

export default StopWatchTimer
