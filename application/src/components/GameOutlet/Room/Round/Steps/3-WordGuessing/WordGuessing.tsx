import React, { FC, memo, useCallback, useEffect, useMemo, useState } from "react"
import { useSocketIo } from "../../../../../../contexts/SocketIo/SocketIoContext"
import { teamEvents, wordEvent } from "../../../../../../_definitions/Events"
import { ITeam } from "../../../../../../_definitions/ITeam"
import { IWord } from "../../../../../../_definitions/IWord"
import { useMe } from "../../../../../../contexts/Me/MeContext"
import { useTeams } from "../../../../../../contexts/Teams/TeamsContext"
import { updateTeam } from "../../../../../../contexts/Teams/TeamsActionCreator"
import { Button, message } from "antd"
import MulticolorTitle from "../../../../../Structure/MulticoloreTitle/MulticolorTitle"
import TypingGradient from "../../../../../Structure/TypingGradient/TypingGradient"
import Chrono from "./Chrono/Chrono"

type WordValidationProps = {
  myTurn : boolean,
  teamTurn : ITeam,
}

const WordGuessing : FC<WordValidationProps> = ({ myTurn, teamTurn }) => {

  const { socket } = useSocketIo()
  const [word, setWord] = useState<IWord>()
  const { me } = useMe()
  const { setTeams } = useTeams()
  const [guessed, setGuessed] = useState<boolean>(false)

  useEffect(()=>{
    socket
      .on(wordEvent.server.CHANGED, (word: IWord, team : ITeam)=> {
        setWord(word)
        setTeams(updateTeam(team))
      })
      .on(wordEvent.server.GUESSED, (word: IWord, team : ITeam)=> {
        message.success("+1 point, THE WORD HAS BEEN GUESSED")
        setWord(word)
        setTeams(updateTeam(team))
      })

    if (teamTurn.wordId) {
      socket.emit(wordEvent.client.GET, ...[teamTurn.wordId, me.roomId], (word: IWord)=>{
        setWord(word)
      })
    }

    return () => {
      socket
        .off(wordEvent.server.CHANGED)
        .off(wordEvent.server.GUESSED)
    }

  }, [me.roomId, setTeams, socket, teamTurn.wordId])

  const canSeeWord : boolean = useMemo(() => !(me.blinded && myTurn),
    [me.blinded, myTurn])

  const handleGuessed = useCallback(()=> {
    if (!myTurn)
      message.warning("Woow !! It's not your turn")
    if (guessed)
      return
    setGuessed(true)
    socket.emit(wordEvent.client.GUESSED, ...[word?.id, me.roomId])
  }, [guessed, me.roomId, myTurn, socket, word?.id])

  const handleTurnEnd = useCallback(()=> {
    if (!myTurn) {
      return message.warning(
        <div>
          <p>"Calm down !! It's not your turn"</p>
          <img alt={""} src="https://media.giphy.com/media/xxhKYiOOIs9mGZz1Hy/giphy.gif" width="250" />
        </div>
      )
    }
    socket.emit(teamEvents.client.TURN_END, ...[me.teamId, me.roomId])
  }, [me.roomId, me.teamId, myTurn, socket])

  return (
    <section style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>

      <div style={{ display: "flex", flexWrap: "wrap-reverse", justifyContent: "space-around", width: "100%" }}>
        {!guessed && <div>
          <MulticolorTitle content={["Guess", "The", "Word"]} />
          <br/>
          {word &&
       <div>
         {canSeeWord
           ? <TypingGradient content={["Hi, the word is", (word.value)]} />
           : <TypingGradient content={["You can't see word", ""]} />
         }
       </div>
          }
          <br/>
          <Button type="primary" style={{ marginRight: "1rem" }} size="large" disabled={!myTurn} onClick={handleGuessed}> GUESSED</Button>
          <Button size="large" disabled={!myTurn} danger onClick={handleTurnEnd} >TURN END</Button>
        </div>}

        <div style={{ marginLeft: "2rem", marginBottom: "3rem" }}>
          <Chrono />
        </div>
      </div>

    </section>
  )
}

export default memo(WordGuessing)
