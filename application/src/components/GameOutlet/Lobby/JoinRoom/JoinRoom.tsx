import React, { FC, memo, useEffect, useState } from "react"
import { Avatar, Button, Card, List, Tag } from "antd"
import { FetchData } from "../../../../services/fetchData"
import { roomEvents } from "../../../../_definitions/Events"
import { IRoom } from "../../../../_definitions/IRoom"
import { useSocketIo } from "../../../../contexts/SocketIo/SocketIoContext"
import useBreakpoint from "antd/es/grid/hooks/useBreakpoint"
import { CheckCircleOutlined, MinusCircleOutlined } from "@ant-design/icons"

type JoinRoomProps = {
  handleJoinRoom: (roomId: string) => void
}

type RoomPick = Pick<IRoom, "id" | "avatar" | "name" | "open">

const JoinRoom: FC<JoinRoomProps> = ({ handleJoinRoom }) => {

  const { sm } = useBreakpoint()

  const { socket } = useSocketIo()
  const [roomList, setRoomList] = useState<RoomPick[]>([])

  useEffect(() => {

    socket.emit(roomEvents.client.LIST, (rooms: RoomPick[])=>{
      if (rooms.length)
        return setRoomList(rooms)

      FetchData.get<{ data: RoomPick[] }>("rooms").then((paylaod) => {
        paylaod ? setRoomList(paylaod.data) : ""
      })
    })

    socket
      .on(roomEvents.server.CREATED, (room: IRoom) => {
        setRoomList((prevList) => [...prevList, room])
      })
      .on(roomEvents.server.DELETED, (roomId: string) => {
        setRoomList((prevList) => prevList.filter((r) => r.id !== roomId))
      })
      .on(roomEvents.server.CLOSED, (room: IRoom) => {
        setRoomList((prevList) => prevList.map((r) =>{
          return r.id !== room.id ? r : room
        }))
      })

    setTimeout(()=>{
      socket.emit(roomEvents.client.LIST, (rooms: RoomPick[])=>{
        setRoomList(rooms)
      })
    }, 500)

    return () => {
      socket
        .off(roomEvents.server.CREATED)
        .off(roomEvents.server.DELETED)
        .off(roomEvents.server.CLOSED)
    }

  }, [socket])

  return (
    <Card
      style={{ marginTop: 16 }}
      type='inner'
      title='Liste des parties'
      bordered={false}
    >
      <List
        size={sm ? "default" : "small"}
        itemLayout={sm ? "horizontal" : "vertical"}
        dataSource={roomList}
        renderItem={(roomPick: RoomPick) => (
          <List.Item
            actions={[
              <Button
                key='join-room'
                onClick={() => handleJoinRoom(roomPick.id)}
                disabled={!roomPick.open}
              >
                Join
              </Button>
            ]}
          >
            <List.Item.Meta
              avatar={
                <Avatar
                  size={{ xs: 15, sm: 25, md: 35, lg: 45, xl: 55, xxl: 65 }}
                  src={roomPick.avatar}
                />
              }
              title={<span>Room : {roomPick.name}</span>}
              description={
                <span>
                  {roomPick.id}{"  "}
                  {roomPick.open
                    ? <Tag icon={<CheckCircleOutlined />} color="success">Open</Tag>
                    : <Tag icon={<MinusCircleOutlined />} color="error">Close</Tag>
                  }
                </span>
              }
            />
          </List.Item>
        )}
      />
    </Card>
  )

}

export default memo(JoinRoom)
