import React, { ChangeEvent, memo, useState } from "react"
import Search from "antd/es/input/Search"
import { Card } from "antd"
import { PlayCircleOutlined } from "@ant-design/icons"
import useBreakpoint from "antd/es/grid/hooks/useBreakpoint"

type CreateRoomProps = {
  handleCreateRoom: (roomName: string) => void
}

function CreateRoom ({ handleCreateRoom }: CreateRoomProps) {

  const { sm } = useBreakpoint()
  const [roomName, setRoomName] = useState<string>("")

  const handleRoomName = (e: ChangeEvent<HTMLInputElement>) => {

    setRoomName(e.target.value)

  }

  return (
    <Card type='inner' title='Create room' bordered={false}>
      <Search
        value={roomName}
        placeholder='Enter room name'
        allowClear={false}
        enterButton={sm ? "Create room" : <PlayCircleOutlined />}
        size='large'
        onChange={handleRoomName}
        onSearch={handleCreateRoom}
      />
    </Card>
  )

}

export default memo(CreateRoom)
