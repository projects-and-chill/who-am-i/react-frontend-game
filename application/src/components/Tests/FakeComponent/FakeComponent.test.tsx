import React from "react"
import { render, screen } from "@testing-library/react"
import FakeComponent from "./FakeComponent"

/**
 * @jest-environment node
 */

test("renders learn react link", () => {
  render(<FakeComponent username={"ezf"} />)
  const titleElement = screen.getByText(/Hey/i)
  expect(titleElement).toBeInTheDocument()
})
