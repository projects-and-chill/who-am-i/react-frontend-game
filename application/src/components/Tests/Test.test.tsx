import React from "react"
import { render, screen } from "@testing-library/react"
import Tests from "./Tests"

test("renders learn react link", () => {
  render(<Tests />)
  const titleElement = screen.getByText(/title/i)
  expect(titleElement).toBeInTheDocument()
})
