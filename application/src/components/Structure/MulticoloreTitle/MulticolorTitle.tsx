import React, { CSSProperties, FC } from "react"
import styleModule from "./MulticolorTitle.module.css"

type Props = {
  content: [string, string?, string?],
  style?: CSSProperties
}

const MulticolorTitle : FC<Props> = ({ content, style = {} }) => {

  return (
    <section className={styleModule.shape}>
      <div className={styleModule.container} style={{ paddingBottom: content[2] ? "1.5rem" : "0rem" }}>
        <h1 className={styleModule.h1} style={{ ...style }}>
          <div className={styleModule.part}> <span>{content[0]}</span> </div>
          <div className={styleModule.part}> <span>{content[1]} </span> </div>
          <div className={styleModule.part}> <span> {content[2]}</span> </div>
        </h1>
      </div>
    </section>
  )
}

export default MulticolorTitle
