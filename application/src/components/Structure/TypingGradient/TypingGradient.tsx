import React, { CSSProperties, FC, memo, useCallback, useEffect, useRef, useState } from "react"
import styleModule from "./TypingGradient.module.css"
import useBreakpoint from "antd/es/grid/hooks/useBreakpoint"

type Props = {
  content: [string, string]
}

const animationName = "shring-animation"

const TypingGradient : FC<Props> = ({ content }) => {

  const breakpoint = useBreakpoint()
  const [fontSize, setFontSize] = useState<number>(0)
  const [display, setDisplay] = useState<boolean>(true)
  const [style, setStyle] = useState<CSSProperties>({})
  const [globalSize, setGlobalSize] = useState<CSSProperties>({ minHeight: 100, minWidth: 200 })
  const primaryElem = useRef<HTMLDivElement>(null)

  const fontSizeScreenWidth = useCallback(() => {
    if (breakpoint.xxl)
      return 100
    else if (breakpoint.xl)
      return 85
    else if (breakpoint.lg)
      return 70
    else if (breakpoint.md)
      return 55
    else if (breakpoint.sm)
      return 40
    else
      return 30

  }, [breakpoint.lg, breakpoint.md, breakpoint.sm, breakpoint.xl, breakpoint.xxl])

  const fontSizeWordLen = useCallback(() => {
    const
      len = 10,
      perc = 100,
      minPerc = 45

    const wordLen = content[1].length
    const additioner = 10

    let newPerc : number

    if (wordLen <= len)
      newPerc = perc
    else
      newPerc = ((wordLen + additioner) * perc) / (len + additioner)

    let invertPerc = 100 - (newPerc - 100)
    invertPerc = invertPerc <= minPerc ? minPerc : invertPerc

    const ratio = invertPerc / 100

    return fontSizeScreenWidth() * ratio

  }, [content, fontSizeScreenWidth])

  const makeStyle = useCallback(()=>{
    const elem = primaryElem.current
    if (!elem)
      return
    const textWidth = elem.clientWidth
    const textHeight = elem.clientHeight
    setGlobalSize({
      minHeight: (textHeight > 100 ? textHeight : 100) + 30,
      minWidth: textWidth > 200 ? textWidth : 200
    })

    const keyframes = `
      @-webkit-keyframes ${animationName} {
      0% {
          background-position: 0 0;
          opacity: 0;
          width: 0;
      }
      1% {
          background-position: 0 0;
          opacity: 1;
          border-right: 1px solid orange;
      }
      50% {
          background-position: ${textWidth / 2.5}px 0;
          opacity: 1;
          border-right: 1px solid orange;
      }
      99% {
          background-position: ${textWidth}px 0;
          opacity: 1;
          width:  ${textWidth}px;
          border-right: 1px solid orange;
      }
      100% {
          background-position: ${textWidth}px 0;
          opacity: 1;
          width: fit-content;
          border-right: 1px solid orange;
      }
      }`

    const keyframes2 = `
      @-webkit-keyframes blink {
      50% {
          border-color: transparent;
      }
      }`

    const styleSheet = document.styleSheets[0]
    styleSheet.insertRule(keyframes, styleSheet.cssRules.length)
    styleSheet.insertRule(keyframes2, styleSheet.cssRules.length)

    setTimeout(()=> {
      setStyle({
        width: `${textWidth}px`,
        animation: `${animationName} 2.2s steps(40,end) 2s forwards, blink .5s step-end infinite alternate`
      })
    }, 0)

  }, [])

  useEffect(()=>{
    setFontSize(fontSizeWordLen)
    makeStyle()
    setTimeout(()=>{
      setDisplay(false)
      setTimeout(()=> {
        setDisplay(true)
      }, 0)
    }, 0)
  }, [fontSize, fontSizeWordLen, makeStyle])

  return (
    <div style={globalSize}>
      {display &&
        <div className={styleModule.container}>
          {fontSize !== 0 &&
          <h1>
            <div className={styleModule.textSecondary}> {content[0]} </div>
            <div ref={primaryElem} style={{ fontSize, ...style }} className={styleModule.textPrimary}>
              {content[1]}
            </div>
          </h1>}
        </div>
      }
    </div>
  )

}

export default memo(TypingGradient)
