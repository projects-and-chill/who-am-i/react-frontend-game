import React, { CSSProperties, FC, memo, useCallback, useEffect, useMemo, useState } from "react"
import "./FlipNumber.css"

type FlipClass = "flip-clock-before" | "flip-clock-active" | "none"

type FlipElementProps = {
  number: number,
  operator:number,
  color: string
}

const FlipElement : FC<FlipElementProps> = memo(({ number, color, operator }) =>{
  const flipElementPart = useCallback((className : FlipClass, score : number) => {
    return (
      <li
        className={className}
      >
        <a>
          <div className="up">
            <div className="shadow"></div>
            <div style={{ backgroundColor: color }} className="inn">{score}</div>
          </div>
          <div className="down">
            <div className="shadow"></div>
            <div style={{ backgroundColor: color }} className="inn">{score}</div>
          </div>
        </a>
      </li>
    )
  }, [color])

  return (<>
    {flipElementPart("flip-clock-before", number > 0 ? number + operator : number)}
    {flipElementPart("flip-clock-active", number)}
  </>)
})
FlipElement.displayName = "Flipelement"

/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
/*-------------------------------------------------------*/

const sizeRefsPx = {
  fontSize: 70,
  lineHeight: 90,
  height: 90,
  width: 60,
  marginRight: 7,
  marginLeft: 7,
  top: 42
}

type Props = {
  number : number,
  fontSize? : number,
  color? : string,
}

const FlipNumber : FC<Props> = memo(({
  number,
  fontSize = sizeRefsPx.fontSize,
  color = "#333"
}) => {

  const [displayUnit, setDisplayUnit] = useState<boolean>(true)
  const [displayTens, setDisplayTens] = useState<boolean>(true)
  const [displayHundreds, setdisplayHundreds] = useState<boolean>(false)
  const [value, setValue] = useState<number>(number)
  const [prevValue, setPrevValue] = useState<number>(number)
  const [operator, setOperator] = useState<number>(-1)

  useEffect(() =>{
    setDisplayUnit(false)
    setValue(number)
    setTimeout(()=>setDisplayUnit(true), 0)

  }, [number])

  useEffect(() =>{
    const op = value > prevValue ? -1 : 1
    setOperator(op)
    setPrevValue(value)

  }, [value]) // eslint-disable-line

  const getNumber = (bigNumber : number, position: number) => {
    const strBigNumber = bigNumber.toString()
    const strNumber = strBigNumber.charAt(strBigNumber.length - position) || "0"
    return parseInt(strNumber, 10)
  }

  const scoreUnits = useMemo(() => {
    return getNumber(value, 1)

  }, [value])

  const scoreTens = useMemo(() => {
    if (value < 10)
      return 0
    const prevTens = getNumber(value - 1, 2)
    const newTens = getNumber(value, 2)
    if (prevTens !== newTens) {
      setTimeout(()=>{
        setDisplayTens(false)
        setTimeout(()=>setDisplayTens(true), 0)
      }, 0)
    }
    return newTens

  }, [value])

  const scoreUndreds = useMemo(() => {
    if (number < 100)
      return 0
    const prevTens = getNumber(number - 1, 3)
    const newTens = getNumber(number, 3)
    if (prevTens !== newTens) {
      setTimeout(()=>{
        setdisplayHundreds(false)
        setTimeout(()=>setdisplayHundreds(true), 0)
      }, 0)
    }
    return newTens
  }, [number])

  const formatToPx = useCallback((elem : number) => `${(elem * fontSize / sizeRefsPx.fontSize)}px`
    , [fontSize]
  )

  const ulStylePx : CSSProperties = useMemo(()=>{
    return {
      fontSize: `${fontSize}px`,
      lineHeight: formatToPx(sizeRefsPx.lineHeight),
      height: formatToPx(sizeRefsPx.height),
      width: formatToPx(sizeRefsPx.width),
      marginRight: formatToPx(sizeRefsPx.marginRight),
      marginLeft: formatToPx(sizeRefsPx.marginLeft),
      marginBottom: 0,
      minWidth: "fit-content",
      backgroundColor: color
    }
  }, [color, fontSize, formatToPx])

  return (
    <section style={{ width: "max-content" }}>

      <style>
        {`.flip-clock-wrapper ul li a div.up:after {
            top: ${formatToPx(sizeRefsPx.top)}
          }
        `}
      </style>

      <div className="flip-clock-wrapper">

        {displayHundreds &&
          <ul style={ulStylePx} className="flip play">
            {displayHundreds && <FlipElement operator={operator} color={color} number={scoreUndreds} />}
          </ul>
        }

        <ul style={ulStylePx} className="flip play">
          {displayTens && <FlipElement operator={operator} color={color} number={scoreTens} /> }
        </ul>

        <ul style={ulStylePx} className="flip play">
          {displayUnit && <FlipElement operator={operator} color={color} number={scoreUnits} /> }
        </ul>

      </div>
    </section>
  )
})
FlipNumber.displayName = "FlipNumber"

export default FlipNumber
