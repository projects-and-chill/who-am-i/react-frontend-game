export class GoogleSearch{

  duckduckgoSearchBar(){
    return cy.get('input[id="search_form_input_homepage"]').first();
  }
  duckduckgoSearchBtn(){
    return cy.get('input[id="search_button_homepage"]').first();
  }
  searchResultH2(){
    return cy.get('h2').first();
  }

  searchResultH1(){
    return cy.get('h1').first();
  }
}
