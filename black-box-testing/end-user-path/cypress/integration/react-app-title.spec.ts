import {ReactLandingPage} from "../page-objects/react-landing-page.page";

const landingPage = new ReactLandingPage();

describe('React pipeline web site', () => {
  beforeEach(()=> {
    cy.visit('/');
  })

  it('should have secret',() => {

    landingPage.buttonShowSecret().click({ force: true });
    landingPage.secretParagraphResult()
      .should("exist")
      .and('include.text',"father")
  });


  it('should title be visible',() => {

    landingPage.landingTitle().should('be.visible')

  });


});
